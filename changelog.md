---
  title: Swimapp Changelog
  subtitle: Lists all changes to each version of Swimapp
  layout: docs.hbs
---

# Version 1.0.0
> Release Date : 21 October 2015

- Initial Submit
