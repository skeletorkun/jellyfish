(function() {
    'use strict';

    angular
        .module('services')
        .service('GeneratorService', GeneratorService);

    /* @ngInject */
    function GeneratorService($log, ConstantsService, PreferencesService) {

        var vm = this;

        vm.currentWorkout = {
            hasKick : false,
            hasDrill : false
        };

        var constants = ConstantsService;
        var settings = PreferencesService;
        vm.generateWorkout = generateWorkout;

        function generateWorkout(){

            vm.currentWorkout = {
                hasKick : false,
                hasDrill : false
            };

            $log.log('Generating workout...');
            settings.print();

            vm.currentWorkout.warmup = generateSet(constants.sets.warmup);
            vm.currentWorkout.buildup = generateSet(constants.sets.buildup);
            vm.currentWorkout.core = generateSet(constants.sets.core);
            vm.currentWorkout.warmdown = generateSet(constants.sets.warmdown);

            vm.currentWorkout.totalDistance =  vm.currentWorkout.warmup.totalDistance +
                                                vm.currentWorkout.buildup.totalDistance +
                                                vm.currentWorkout.core.totalDistance +
                                                vm.currentWorkout.warmdown.totalDistance;

            print(vm.currentWorkout);

            return vm.currentWorkout;
        };

        function generateSet(setType){

            var set = {};
            set.exercises = [];
            set.totalDistance = 0;
            var targetDuration = settings.duration * constants.durationMultiplier[setType];
            var targetDistance = getSetLength(setType);

            $log.log('Generating set : ' + setType + ' target duration: ' + targetDuration);

            var exerciseCount = getExerciseCount(setType);
            var exDuration = targetDuration / exerciseCount;

            while(targetDuration  + constants.durationBuffer> 0 && set.exercises.length < exerciseCount && set.totalDistance < targetDistance){

                var ex = generateExercise(exDuration, setType);
                targetDuration -= ex.expectedTime;
                set.totalDistance += ex.len * ex.repetition;
                set.exercises.push(ex);
            }

            return set;
        }

        function getSetLength(setType){

            var len = 0;
            switch(setType){

                case constants.sets.warmup:
                case constants.sets.warmdown:
                    len = settings.duration == constants.workoutDurations.short ?  200 : 400;
                    break;

                case constants.sets.buildup:
                    len = settings.duration == constants.workoutDurations.short ?  400 : 600;
                    break;
                case constants.sets.core:
                    len = settings.duration == constants.workoutDurations.short ?  1000 : 1200;
                    if(settings.easyDay == true) len -= 200;
                    break;
            }

            return len;
        }


        function getExerciseCount(setType){

            switch(setType){

                case constants.sets.warmup:
                case constants.sets.buildup:
                case constants.sets.warmdown:
                    return getRandElem([2,3]);
                case constants.sets.core:
                    return getRandElem([2,3,4]);
                default:
                    $log.log('unknown set type: ' + setType);
                    return 3;
            }
        }

//        function generateDrill(duration){
//            var e = {};
//            e.swim = constants.swims.kick;
//            e.stroke = getStroke(constants.sets.buildup);
//            e.equipment = getEquipment(e);
//            e.len = getLength(e);
//            e.effort = getEffort(e);
//            e.rest = getRest(e.len);
//            e.expectedTime = getLapTime(e);
//            e.repetition = getRepetition(duration, e.expectedTime, e.len);
//
//            vm.currentWorkout.hasDrill = true;
//            print(e);
//            return e;
//        }

        function generateKick(duration){
            var e = {};
            e.set = constants.sets.buildup;
            e.swim = constants.swims.kick;
            e.stroke = getStroke(constants.sets.buildup);
            e.len = getLength(e);
            e.effort = getEffort(e);
            e.rest = getRest(e.len);
            e.equipment = getEquipmentForKick(e);
            e.expectedTime = getLapTime(e);
            e.repetition = getRepetition(duration, e.expectedTime, e.len);
            e.text = getAsText(e);

            vm.currentWorkout.hasKick = true;

            print(e);
            return e;
        }

        function generateExercise(duration, setType){

            if(setType == constants.sets.buildup && !vm.currentWorkout.hasKick) return generateKick(duration);
//            if(setType == constants.sets.buildup && !vm.currentWorkout.hasDrill) return generateDrill(duration);

            var e = {};
            e.set = setType
            e.swim = constants.swims.swim;
            e.stroke = getStroke(setType);
            e.len = getLength(e);
            e.effort = getEffort(e);
            e.rest = getRest(e.len); //, e.effort
            e.equipment = getEquipment(e); //setType, stroke
            e.expectedTime = getLapTime(e);
            e.repetition = getRepetition(duration, e.expectedTime, e.len);

            e.text = getAsText(e);

            print(e);
            return e;
        }

        function getAsText(e){

            var str = e.repetition + ' x ' + e.len + ' ' + e.stroke;

            // add kick / drill to text but not 'swim'
            if(e.swim != constants.swims.swim)
                str += ' ' + e.swim;

            if(e.equipment)
                str += " with " + e.equipment;

            return str;
        }

        function getRepetition(duration, expectedTime, len){

            var rep = parseInt(duration / expectedTime) || 1;

            // avoid : 5 x 25, 3 x 25 ..etc.
            if(len == 25 && rep % 2 == 1) rep++;

            return rep;
        }

        function getLapTime(e){

            var time = constants.getDurationPerStrokeLen(e.stroke, e.len);
            time *= constants.swimmerLevel.average;
            time += e.rest;
            time *= constants.getDurationFactorPerEquipment(e.equipment);
            time *= constants.getDurationFactorPerSwim(e.swim);

            return parseInt(time);
        }

        function getEquipmentForKick(){

            //only kickBoard / fins are allowed
            var kb = constants.equipments.kickBoard;
            var fins = constants.equipments.fins;

            var kickEquipments = [];

            if(settings.selectedEquipments[kb]) kickEquipments.push(kb);
            if(settings.selectedEquipments[fins]) kickEquipments.push(fins);

            return getRandElem(kickEquipments);
        }

        function getEquipment(e){

            //no equip in warmup/warmdown
            if(e.set == constants.sets.warmup || e.set == constants.sets.warmdown )
                return;

            //skip if no equip selected
            if(!settings.selectedEquipments || !settings.useEquipment)
                return;

            var eq = getRandProp(constants.equipments);

            //available equipments
            if(settings.selectedEquipments[eq] == false)
                return getEquipment(e);

            // bands are only for crawl
            if(eq == constants.equipments.bands && e.stroke != constants.strokes.crawl)
                return;

            //no fins for breaststroke
            if(eq == constants.equipments.fins && e.stroke == constants.strokes.breaststroke)
                return;

            //kickboard is only for kick
            if(eq == constants.equipments.kickBoard && e.swim != constants.swims.kick)
                return;

            eq = getRandElem([1,2,3]) == 1 ? eq : null;

            return eq;
        }

        function getRest(length){

            var rest;
            switch(length){
                case 25:
                    rest = 5;
                    break;
                case 50:
                    rest = 15;
                    break;
                default:
                    rest = 20;
                    break;
            }

            return rest;
        }

        function getEffort(e){

            if(e.swim != constants.swims.swim || e.set != constants.sets.core){
                return constants.efforts.easy;
            }

            var eff = getRandProp(constants.efforts);

            return eff;
        }

        function getLength(e){

            var len = getRandElem(constants.lengths);

            // 200m not allowed for warmup / warmdown / butterfly / kicks & drills
            if(len == 200){
                if(e.set == constants.sets.warmup || e.set == constants.sets.warmdown)
                    return getLength(e);

                if(e.stroke == constants.strokes.butterfly)
                    return getLength(e);

                if(e.swim == constants.swims.kick || e.swim == constants.swims.drill)
                    return getLength(e);
            }

            // 100m not allowed fo butterfly / kick
            if(len == 100){
                if(e.stroke == constants.strokes.butterfly)
                    return getLength(e);

                if(e.swim == constants.swims.kick)
                    return getLength(e);
            }

            if(len == 50){
                if(settings.poolSize == 33)
                    return getLength(e);
            }

            if(len == 33){
                if(settings.poolSize != 33)
                    return getLength(e);
            }

            if(len == 25){
                //no 25m swim in core
                if(e.set == constants.sets.core)
                    return getLength(e);

                if(settings.poolSize != 25)
                    return getLength(e);
            }

            return len;
        }

        function getStroke(setType, tried){

            var stroke = getRandProp(constants.strokes);
            var favoredStroke = settings.favoredStroke;
            var i = tried || 0;

            //available strokes
            if(settings.selectedStrokes[stroke] == false && stroke != constants.strokes.any)
                return getStroke(setType, i);

            // avoid butterfly in warmup/warmdown
            if(stroke == constants.strokes.butterfly &&
                (setType != constants.sets.core && setType != constants.sets.buildup) )
                return getStroke(setType, i);

            // try 3 times to hit the favored stroke
            if(favoredStroke && favoredStroke != stroke && i < 2)
                return getStroke(setType, ++i);

            return stroke;
        }


        function print(workout){
            $log.log(angular.toJson(workout));
        }

        function getRandom(max) {
            return Math.floor(Math.random() * max);
        }

        function getRandElem(elems){
            return elems[getRandom(elems.length)];
        }

        function getRandProp(obj){
            var fields = [];
            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                    fields.push(key);
                }
            }

            var r = getRandom(fields.length);
            var f = fields[r];

            return obj[f];
        }
    }
})();
