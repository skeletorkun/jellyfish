(function() {
    'use strict';

    angular
        .module('services')
        .service('WorkoutService', WorkoutService);

    /* @ngInject */
    function WorkoutService(LocalStorageService) {

        var vm = this;
        vm.currentWorkout = LocalStorageService.getObject('currentWorkout');
        vm.setCurrentWorkout = setCurrentWorkout;
        vm.getCurrentWorkout = getCurrentWorkout;

        function getCurrentWorkout(){
            return vm.currentWorkout;
        }

        function setCurrentWorkout(workout){
            vm.currentWorkout = workout;

            LocalStorageService.setObject('currentWorkout', workout);
        }



    }


})();
