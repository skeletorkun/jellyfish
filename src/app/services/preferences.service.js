(function() {
    'use strict';

    angular
        .module('services')
        .service('PreferencesService', PreferencesService);

    /* @ngInject */
    function PreferencesService($log, ConstantsService, LocalStorageService) {

        var vm = this;

        //functions
        vm.getEquipments = getEquipments;
        vm.getStrokes = getStrokes;
        vm.print = print;
        vm.save = save;

        //properties
        vm.units = 'Meters';
        vm.duration = ConstantsService.workoutDurations.short;
        vm.poolSize = ConstantsService.poolSize.small;
        vm.workoutType = 'Any';
        vm.useEquipment = true;
        vm.easyDay = false;
        vm.selectedEquipments = getEquipments(); //init
        vm.selectedStrokes = getStrokes(); //init
        vm.favoredStroke = ConstantsService.strokes.crawl;

        var savedPrefs = LocalStorageService.getObject('preferences');

        if(savedPrefs.units){

            vm.units = savedPrefs.units;
            vm.duration = savedPrefs.duration;
            vm.poolSize = savedPrefs.poolSize;
            vm.workoutType = savedPrefs.workoutType;
            vm.useEquipment = savedPrefs.useEquipment;
            vm.easyDay = savedPrefs.easyDay;
            vm.selectedEquipments = savedPrefs.selectedEquipments;
            vm.selectedStrokes = savedPrefs.selectedStrokes;
            vm.favoredStroke = savedPrefs.favoredStroke;
        }


        function getEquipments(){

            var equipments = {};
            for(var key in ConstantsService.equipments){
                var str = ConstantsService.equipments[key];
                equipments[str] = true;
            }

            $log.log('equipments ' + equipments);
            return equipments;
        };

        function getStrokes(){

            var strokes = {};
            for(var key in ConstantsService.strokes){

                var str = ConstantsService.strokes[key];
                if(str == ConstantsService.strokes.any) continue;

                strokes[str] = true;
            }

            $log.log('strokes ' + strokes);
            return strokes;
        };

        function save(){
            LocalStorageService.setObject('preferences', vm);

            $log.log('preferences saved');
            vm.print();
        };

        function print(){
            $log.log('preferences : ' + angular.toJson(vm));
        };

    }
})();
