(function() {
    'use strict';

    angular
        .module('services')
        .service('ConstantsService', ConstantsService);

    /* @ngInject */
    function ConstantsService() {

        return {

            // duration multipliers
            durationMultiplier : { warmup: 0.125, buildup : 0.25, core: 0.5, warmdown: 0.125 },

            workoutDurations: { short: 40 * 60, long: 55*60 },
            workoutDistances: { short: 1800, long: 2400 },

            durationBuffer : 60 * 3, // 3 mins

            //set types
            sets : {warmup : 'warmup', buildup: 'buildup', core: 'core', warmdown : 'warmdown'},

            //stroke types
            strokes: { crawl : 'Crawl', butterfly: 'Butterfly', backstroke: 'Backstroke', breaststroke: 'Breaststroke', any: 'Any stroke'},

            //lengths
            lengths : [25, 33, 50, 100, 200],

            //swim types
            swims : { swim: 'swim', kick: 'kick' , drill: 'drill'},

            equipments : { kickBoard: 'Kickboard', paddles: 'Paddles', pullBuoy: 'Pull buoy', fins: 'Fins', bands: 'Bands'},

            efforts : { easy: 'Easy', aerobic: 'Aerobic', effort: 'Effort'},

            //pool sizes
            poolSize: { small : 25, medium: 33, olympic : 50},

            swimmerLevel : {

                slow: 1.2,
                average: 1,
                fast: 0.8
            },
            getDurationFactorPerSwim : function(swim){

               switch(swim){
                   case this.swims.kick:
                       return 1.3;
                   case this.equipments.drill:
                       return 1.2;
                   default:
                       return 1;
               }
            },
            getDurationFactorPerEquipment : function(equipment){

                switch(equipment){
                    case this.equipments.paddles:
                        return 0.9;
                    case this.equipments.fins:
                        return 0.8;
                    default:
                        return 1;
                }
            },

            getDurationPerStrokeLen : function(stroke, len, swim){

                switch(stroke | len){
                    case this.strokes.crawl | 25:
                        return 22;
                    case this.strokes.crawl | 33:
                        return 35;
                    case this.strokes.crawl | 50:
                        return 50;
                    case this.strokes.crawl | 100:
                        return 100;
                    case this.strokes.crawl | 200:
                        return 220;

                    case this.strokes.backstroke | 25:
                        return 30;
                    case this.strokes.backstroke | 33:
                        return 50;
                    case this.strokes.backstroke | 50:
                        return 65;
                    case this.strokes.backstroke | 100:
                        return 140;
                    case this.strokes.backstroke | 200:
                        return 300;

                    case this.strokes.butterfly | 25:
                        return 30;
                    case this.strokes.butterfly | 33:
                        return 50;
                    case this.strokes.butterfly | 50:
                        return 70;
                    case this.strokes.butterfly | 100:
                        return 150;
                    case this.strokes.butterfly | 200:
                        return 330;

                    case this.strokes.breaststroke | 25:
                        return 30;
                    case this.strokes.breaststroke | 33:
                        return 50;
                    case this.strokes.breaststroke | 50:
                        return 65;
                    case this.strokes.breaststroke | 100:
                        return 140;
                    case this.strokes.breaststroke | 200:
                        return 300;

                    case this.strokes.any | 25:
                        return 30;
                    case this.strokes.any | 33:
                        return 50;
                    case this.strokes.any | 50:
                        return 65;
                    case this.strokes.any | 100:
                        return 140;
                    case this.strokes.any | 200:
                        return 300;

                }
            }
        };
    }


})();
