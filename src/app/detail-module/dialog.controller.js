(function() {
    'use strict';

    angular
        .module('detail-module')
        .controller('DetailDialogController', DetailDialogController);

    /* @ngInject */
    function DetailDialogController($state, $mdDialog, $log, PreferencesService, GeneratorService) {
        var vm = this;
        vm.cancel = cancel;
        vm.hide = hide;
        vm.workout = {};

        vm.preferences = PreferencesService;
        vm.settingsChanged = settingsChanged;
        vm.selectedEquipmentsText = getAsText(PreferencesService.selectedEquipments);


        /////////////////////////

        function hide() {
            vm.workout = GeneratorService.generateWorkout();
            $log.log('workout: ' + vm.workout);
            $mdDialog.hide(vm.workout);
            PreferencesService.save();
            PreferencesService.print();
        }

        function getAsText(list){
            var selected = [];

            for(var key in list){
                if(list[key] == true){
                    selected.push(key);
                }
            }

            return selected.join(', ');
        }


        function cancel() {
            $mdDialog.cancel();
            PreferencesService.print();
        }

        function settingsChanged(){
            PreferencesService.save();
        }
    }
})();
