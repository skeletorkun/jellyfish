(function() {
    'use strict';

    angular
        .module('detail-module', [
            'services'
        ]);
})();
