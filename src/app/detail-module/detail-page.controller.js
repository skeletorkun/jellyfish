(function() {
    'use strict';

    angular
        .module('detail-module')
        .controller('DetailPageController', DetailPageController);

    /* @ngInject */
    function DetailPageController($scope, $mdDialog, $log, GeneratorService, ConstantsService, WorkoutService) {
        var vm = this;
        vm.currentWorkout = WorkoutService.getCurrentWorkout();
        vm.title = generateTitle(vm.currentWorkout);
        vm.isEmpty = isEmpty;

        function isEmpty(){
            if(!vm.currentWorkout.totalDistance)
                return true;
            return false;
        }

        function generateTitle(aWorkout){

            var title = {};
            title.distance = 'Total : ' + aWorkout.totalDistance + 'm';
            title.duration = aWorkout.duration == ConstantsService.workoutDurations.short ? '30 - 45 mins' : '45 - 60 mins';
            return title;
        }

        function addWorkout( ev ){
            $mdDialog.show({
                templateUrl: 'app/detail-module/add-workout-dialog.tmpl.html',
                targetEvent: ev,
                controller: 'DetailDialogController',
                controllerAs: 'vm'
            })
            .then(function(workout) {
                WorkoutService.setCurrentWorkout(workout);
                vm.currentWorkout = workout;
                vm.title = generateTitle(workout);
            });
        }

        // listeners
        $scope.$on('addWorkout', addWorkout);
    }


})();
