(function() {
    'use strict';

    angular
        .module('detail-module')
        .controller('DetailFabController', DetailFabController);

    /* @ngInject */
    function DetailFabController($rootScope) {
        var vm = this;
        vm.addWorkout = addWorkout;

        ////////////////

        function addWorkout($event) {
            $rootScope.$broadcast('addWorkout', $event);
        }
    }
})();
