(function() {
    'use strict';

    angular
        .module('detail-module')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($translatePartialLoaderProvider, $stateProvider, triMenuProvider) {
        $translatePartialLoaderProvider.addPart('app/detail-module');

        $stateProvider
        .state('triangular.admin-default.detail-page', {
            url: '/detail-module/detail-page',
            views: {
                '': {
                    templateUrl: 'app/detail-module/detail-page.tmpl.html',
                    controller: 'DetailPageController',
                    controllerAs: 'vm'
                },
                'belowContent': {
                    templateUrl: 'app/detail-module/fab-button.tmpl.html',
                    controller: 'DetailFabController',
                    controllerAs: 'vm'
                }
            }
        });

        triMenuProvider.addMenu({
            name: 'MENU.DETAIL.DETAIL-MODULE',
            icon: 'zmdi zmdi-assignment',
            type: 'link',
            priority: 1.1,
            state: 'triangular.admin-default.detail-page'
        });
    }
})();
