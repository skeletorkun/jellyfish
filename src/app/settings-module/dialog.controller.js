(function() {
    'use strict';

    angular
        .module('settings-module')
        .controller('DialogController', DialogController);

    /* @ngInject */
    function DialogController($scope, $mdDialog, $log, PreferencesService, ConstantsService) {
        var vm = this;
        vm.hideEquipments = hideEquipments;
        vm.hideStrokes = hideStrokes;

        vm.equipments = PreferencesService.selectedEquipments;
        vm.strokes = PreferencesService.selectedStrokes;

        /////////////////////////

        function hideEquipments() {
            $mdDialog.hide();
            PreferencesService.selectedEquipments = vm.equipments;
            PreferencesService.save();
            PreferencesService.print();
        }

        function hideStrokes() {
            $mdDialog.hide();
            PreferencesService.selectedStrokes = vm.strokes;
            PreferencesService.save();
            PreferencesService.print();
        }

    }
})();
