(function() {
    'use strict';

    angular
        .module('settings-module')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($translatePartialLoaderProvider, $stateProvider, triMenuProvider) {
        $translatePartialLoaderProvider.addPart('app/settings-module');

        $stateProvider
        .state('triangular.admin-default.settings-page', {
            url: '/settings-module/settings-page',
            views: {
                '': {
                    templateUrl: 'app/settings-module/settings-page.tmpl.html',
                    controller: 'SettingsPageController',
                    controllerAs: 'vm'
                }
            }
        });

        triMenuProvider.addMenu({
            name: 'MENU.SETTINGS.SETTINGS-MODULE',
            icon: 'fa fa-cog',
            type: 'link',
            priority: 2.1,
            state: 'triangular.admin-default.settings-page'
        });
    }
})();
