(function() {
    'use strict';

    angular
        .module('settings-module')
        .controller('SettingsPageController', SettingsPageController);

    /* @ngInject */
    function SettingsPageController($mdDialog, PreferencesService) {
        var vm = this;

        vm.preferences = PreferencesService;

        vm.createEquipmentsDialog = createEquipmentsDialog;
        vm.createStrokesDialog = createStrokesDialog;

        vm.selectedEquipmentsText = getAsText(vm.preferences.selectedEquipments);
        vm.selectedStrokesText = getAsText(vm.preferences.selectedStrokes);
        vm.settingsChanged = settingsChanged;

        function createEquipmentsDialog($event, dialog) {

            $mdDialog.show({
                templateUrl: 'app/settings-module/select-equipment-dialog.tmpl.html',
                targetEvent: $event,
                controller: 'DialogController',
                controllerAs: 'vm'
            })
            .then(function() {
                vm.selectedEquipmentsText = getAsText(vm.preferences.selectedEquipments);
            });
        }

        function createStrokesDialog($event, dialog) {

            $mdDialog.show({
                templateUrl: 'app/settings-module/select-stroke-dialog.tmpl.html',
                targetEvent: $event,
                controller: 'DialogController',
                controllerAs: 'vm'
            })
            .then(function() {
                vm.selectedStrokesText = getAsText(vm.preferences.selectedStrokes);
            });
        }


        function getAsText(list){
            var selected = [];

            for(var key in list){
                if(list[key] == true){
                    selected.push(key);
                }
            }

            return selected.join(', ');
        }

        function settingsChanged(){
            PreferencesService.save();
        }


    }
})();
