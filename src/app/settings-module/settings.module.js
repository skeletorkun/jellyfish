(function() {
    'use strict';

    angular
        .module('settings-module', [
            'services'
        ]);
})();
